﻿# filename: camera_configs.py
import cv2
import numpy as np

left_camera_matrix = np.array([[415.89902, 0., 357.08524],
                               [0., 416.29318, 254.82091],
                               [0., 0., 1.]])
left_distortion = np.array([[-0.38262, 0.15768, 0.00431, -0.00165, 0.00000]])



right_camera_matrix = np.array([[419.91981, 0., 357.30619],
                                [0., 419.48924, 260.87403],
                                [0., 0., 1.]])
right_distortion = np.array([[-0.38804, 0.15918, 0.00461, 0.00182, 0.00000]])

om = np.array([-0.00411, 0.00830, -0.00015]) # 旋转关系向量
R = cv2.Rodrigues(om)[0]  # 使用Rodrigues变换将om变换为R
T = np.array([-57.73538, -0.13820, 0.78562]) # 平移关系向量

size = (640, 480) # 图像尺寸

# 进行立体更正
R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(left_camera_matrix, left_distortion,
                                                                  right_camera_matrix, right_distortion, size, R,
                                                                  T)
# 计算更正map
left_map1, left_map2 = cv2.initUndistortRectifyMap(left_camera_matrix, left_distortion, R1, P1, size, cv2.CV_16SC2)
right_map1, right_map2 = cv2.initUndistortRectifyMap(right_camera_matrix, right_distortion, R2, P2, size, cv2.CV_16SC2)
