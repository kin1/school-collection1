﻿import cv2
import numpy as np #numpy函式庫
import time
import camera_configs #相機校正參數
import paho.mqtt.client as mqtt #mqtt函式庫
import json #json函式庫

#--------------------------------------------------
#MQTT連線設定
ServerIP = "192.168.100.158"
ServerPort = 1883 #port
TopicName = "M10513012" #TOPIC name
mqttc = mqtt.Client("M10513012")
mqttc.connect(ServerIP, ServerPort, 60)
#--------------------------------------------------
#設定影像大小
cap = cv2.VideoCapture(0)
cap.set(3,960)
cap.set(4,1280)

while(1):
    ret, frame = cap.read()#讀取影像
    #分割影像
    frame1 = frame[0:480, 0:640]#左影像
    frame2 = frame[0:480, 640:1280]#右影像

    #cv2.imshow('frame1',frame1)
        
    # 根据更正map对图片进行重构
    img1_rectified = cv2.remap(frame1, camera_configs.left_map1, camera_configs.left_map2, cv2.INTER_LINEAR)#左校正後影像
    img2_rectified = cv2.remap(frame2, camera_configs.right_map1, camera_configs.right_map2, cv2.INTER_LINEAR)#右校正後影像
    #複製影像  
    frame3 = img1_rectified.copy()
    
    if ret :
        # Convert BGR to HSV 轉HSV格式
        hsv = cv2.cvtColor(frame3, cv2.COLOR_BGR2HSV)
        
        # define range1 of Red color in HSV 紅色值範圍
        lower_red = np.array([160,110,60])
        upper_red = np.array([180,255,255])
        # Threshold the HSV image to get only Red colors
        mask0 = cv2.inRange(hsv, lower_red, upper_red)
        
        # define range2 of Red color in HSV 紅色值範圍
        lower_red = np.array([0,110,60])
        upper_red = np.array([2,255,255])    
        # Threshold the HSV image to get only Red colors
        mask1 = cv2.inRange(hsv, lower_red, upper_red)
        mask2 = cv2.bitwise_or(mask1,mask0)#red

        # define range of Blue color in HSV 藍色值範圍
        lower_blue = np.array([105,35,60])
        upper_blue = np.array([130,255,255])
        # Threshold the HSV image to get only blue colors
        mask21 = cv2.inRange(hsv, lower_blue, upper_blue)
        mask22 = cv2.bitwise_or(mask21,mask2)#blue+red

        # define range of Green color in HSV 綠色值範圍
        lower_green = np.array([40,60,60])
        upper_green = np.array([90,255,255])     
        # Threshold the HSV image to get only blue colors
        mask23 = cv2.inRange(hsv, lower_green, upper_green)

        mask24 = cv2.bitwise_or(mask22,mask23)#blue+red+green

        mask3 = mask2.copy()#red 紅
        gray = cv2.GaussianBlur(mask3, (17, 17), 0)#高斯濾波
        edged = cv2.Canny(gray, 90, 130)#物體外框
        contours = cv2.findContours(edged.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#找物體輪廓
        #contours2 = cv2.findContours(mask3,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)[0]
        contours1 = sorted(contours[0], key = cv2.contourArea,reverse = True)#物體輪廓(大->小)
        
        mask4 = mask23.copy()#green 綠
        gray1 = cv2.GaussianBlur(mask4, (5, 5), 0)#高斯濾波
        edged1 = cv2.Canny(gray1, 90, 130)#物體外框
        contours2 = cv2.findContours(edged1.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#找物體輪廓
        contours3 = sorted(contours2[0], key = cv2.contourArea,reverse = True)#物體輪廓(大->小)
        
        mask5 = mask21.copy()#blue 藍
        gray2 = cv2.GaussianBlur(mask5, (17, 17), 0)#高斯濾波
        edged2 = cv2.Canny(gray2, 90, 130)#物體外框
        contours4 = cv2.findContours(edged2.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)#找物體輪廓
        contours5 = sorted(contours4[0], key = cv2.contourArea,reverse = True)#物體輪廓(大->小)
        
        if (len(contours3) > 0):
            
            if (len(contours1) > 0):
                x,y,w,h = cv2.boundingRect(contours1[0])#RED 取出紅色物體座標
                cx = x+w/2
                cy = y+h
                if (x > 0 | y > 0):
                    print "--------------------"
                    print "red:"
                    print "x:%d"%cx# 紅x座標值
                    print "y:%d"%cy# 紅y座標值
                    cv2.drawContours(frame3, contours1, -1, (0, 0, 255), 2)#red 畫紅色輪廓
                    print "--------------------"
            else:
                cx = 0
                print "x:%d"%cx
                cy = 0
                print "y:%d"%cy
            
            #-----------------------------------------------------------------
            
            xc,yc,wc,hc = cv2.boundingRect(contours3[0])#GREEN 取出綠色物體座標
            cxc = xc+wc/2
            if (cxc > 0 | yc > 0):
                print "++++++++++++++++++++"
                print "green:"
                print "xc:%d"%cxc # 機械手臂(綠)x座標值
                print "yc:%d"%yc  # 機械手臂(綠)y座標值
                cv2.drawContours(frame3, contours3, -1, (0, 255, 0), 2)#green 畫綠色輪廓
                print "++++++++++++++++++++"
           #------------------------------------------------------------------
            
            if (len(contours5) > 0):
                xb,yb,wb,hb = cv2.boundingRect(contours5[0])#blue 取出藍色物體座標
                bx = xb+wb/2
                by = yb+hb
                if (bx > 0 | by > 0):
                    print "////////////////////"
                    print "blue:"
                    print "x:%d"%bx # 藍x座標值
                    print "y:%d"%by # 藍y座標值
                    cv2.drawContours(frame3, contours5, 0, (255, 0, 0), 2)#blue 畫藍色輪廓
                    print "////////////////////"
            else:
                bx = 0
                print "xb:%d"%bx
                by = 0
                print "yb:%d"%by
            
            r = len(contours[0])#紅物體數量
            print r
            print "++++++++++++++++++++"
            b = len(contours4[0])#藍物體數量
            print b
            s = len(contours[0])+ len(contours4[0])#(紅+藍)物體總數量
            print "++++++++++++++++++++"
            print s
            print "--------------------"
            
            mqtt_msg = json.dumps({"x":cx,"y":cy,"xc":cxc,"yc":yc,"xb":bx,"yb":by,"s":s})#把訊息存成json格式
            mqttc.publish(TopicName, mqtt_msg, qos=0)#發布訊息
            del cx,cy,cxc,yc,bx,by,s#刪除資料
             
        cv2.imshow('frame',frame3)#顯示影像
        #cv2.imshow('mask3',edged2)#顯示外框
        #cv2.imshow('frame0',frame1)#原影像
        #cv2.imshow('frame1',img1_rectified)#校正後影像
        #print(len(contours[0]))
    
    key = cv2.waitKey(60)
    if key == ord("q"):
        break
    
cv2.destroyAllWindows()


        
