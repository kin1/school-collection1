import RPi.GPIO as GPIO #GPIO函式庫
import paho.mqtt.client as mqtt #mqtt函式庫
import time
import json #json函式庫

#------------------------------------------
#設定mqtt連線
def on_publish(client, userdata, mid):
    print "message"
    
def on_connect(client, userdata, flags, rc):#mqtt連線
    print("Connected with result code "+str(rc))
    client.subscribe("M10513012", qos=0)

def on_message(client, userdata, msg):#mqtt傳輸訊息

    global x, xc, xb, y, yc, yb, s#全域
    payload =json.loads(msg.payload)#接收json格式
    x = payload['x']#存數值
    #print(x)
    y = payload['y']
    #print(y)
    xc = payload['xc']
    print(xc)
    xb = payload['xb']
    #print(xb)
    yc = payload['yc']
    #print(yc)
    yb = payload['yb']
    
    s = payload['s']
    #print(s)
    #time.sleep(0.1)  

client = mqtt.Client()
#client.on_publish = on_publish
client.on_connect = on_connect
client.on_message = on_message
client.connect("192.168.100.158", 1883, 60)
#------------------------------------------
#------------------------------------------
#GPIO設定腳位
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
#頻率
frequency_hertz = 50

pwm = GPIO.PWM(11, frequency_hertz)#<> 前進 後退
pwm1 = GPIO.PWM(13, frequency_hertz)#catch up爪子上升
pwm2 = GPIO.PWM(12, frequency_hertz)#catch 抓取
#pwm3 = GPIO.PWM(15, frequency_hertz)#?
pwm4 = GPIO.PWM(16, frequency_hertz)#up down 上下移動
pwm5 = GPIO.PWM(18, frequency_hertz)#turn 轉向

#<>前進 後退 脈波設定
leftposition = 1.1
rightposition = 1.7
leftpositionnew= 1.15
leftpositionnew1= 1.3
#middleposition = (rightposition - leftposition) / 2 + leftposition
#catch up 爪子上升 脈波設定
leftposition1 = 1.3
rightposition1 = 1.1
midposition1 = 1.2
#catch 抓取 脈波設定
leftposition2 = 1.35
rightposition2 = 2.1
#up1 down1
#leftposition3 = 1.4
#up down 上下移動 脈波設定
leftposition4 = 1.25
rightposition4 = 1.55
#turn 轉向 脈波設定
midposition5 = 1.4
rightposition5 = 0.6
middleposition5 = 1.4
leftposition5 = 2.3

#move 移動位置設定
#>< 前進 後退
positionList = [leftposition]#red >
positionList6 = [leftpositionnew]
#positionList12 = [leftpositionnew1]#blue >
#catch up 爪子上升
positionList1 = [rightposition1]
positionList8 = [leftposition1]
positionList14 = [midposition1]
#catch  抓取
positionList2 = [leftposition2]
positionList7 = [rightposition2]
#up1 down1 
#positionList3 = [leftposition3]
#up down 上下移動
positionList4 = [leftposition4]
positionList10 = [rightposition4]
#turn 轉向
positionList5 = [rightposition5]#red turn
positionList9 = [middleposition5]#起點
positionList13 = [leftposition5]#blue turn
positionList11 = [midposition5]#回到原點

ms_per_cycle = 1000 / frequency_hertz

#接收資料
client.loop_start()
time.sleep(0.1)
client.loop_stop()
#取出總共幾個物體數量
s1 = s
s2 = s1
#系統
for i in range(s):
    c = 0 #轉彎次數
    ss = 0 #前進次數
    xx=0 #前進次數
    #開啟馬達
    pwm4.start(0)
    pwm5.start(0)
    pwm.start(0)
    pwm1.start(0)
    pwm2.start(0)
    #接收資料
    client.loop_start()
    time.sleep(0.2)
    client.loop_stop()
    #red 只存一次紅色座標值
    x1 = x
    print x1
    y1 = y
    #blue 只存一次紅色座標值
    x2 = xb
    print x2
    y2 = yb
    #接收資料
    client.loop_start()
    time.sleep(0.5)

    if(x1 > 0):#如果有紅色物體
    #red green 紅色移動方式
        while((xc-45) > x1):#left direction 左轉
            for position9 in positionList9:
                dutyCyclepercentage = position9 * 100 /ms_per_cycle
                print "position:" +str(position9)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm5.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList9[0] = positionList9[0] + 0.01
                c = c+1
                    
            for position in positionList:#<> 前進
                dutyCyclepercentage = position * 100 /ms_per_cycle
                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList[0] = positionList[0] + 0.01
                xx = xx+1
                
        while((xc+45) < x1): #right direction 右轉
            for position9 in positionList9:
                dutyCyclepercentage = position9 * 100 /ms_per_cycle
                print "position:" +str(position9)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm5.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList9[0] = positionList9[0] - 0.01
                c = c+1

            for position in positionList:#<> 前進
                dutyCyclepercentage = position * 100 /ms_per_cycle
                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList[0] = positionList[0] + 0.01
                xx = xx+1
                
        while((yc-y1) > 30):
            for position in positionList:#<> 前進
                dutyCyclepercentage = position * 100 /ms_per_cycle
                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList[0] = positionList[0] + 0.1
                ss = ss+1
                        
            for position in positionList8:#catch up 爪子上升
                dutyCyclepercentage = position * 100 /ms_per_cycle
                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm1.start(dutyCyclepercentage)
                time.sleep(0.2)
                
            if((yc-y1) <= 30):
                del x,xc,y,yc#刪除數值
                client.loop_stop()#停止收資料
                del x1,y1 #刪除數值
                s1 = s1 - 1 #總物體數扣1
                break
            
        #回歸原本數值位置
        positionList[0] = positionList[0] - (0.01*xx) - (0.1*ss)
        if(positionList9[0] > 1.4):
            positionList9[0] = positionList9[0] - (0.01*c)
        if(positionList9[0] < 1.4):
            positionList9[0] = positionList9[0] + (0.01*c)
        #-------------------------------------------------------------
        
        #fixed move 固定動作
        for position2 in positionList2:#catch 抓取
            dutyCyclepercentage = position2 * 100 /ms_per_cycle
            print "position:" +str(position2)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm2.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position4 in positionList4:#up 上升
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
                        
        for position in positionList6:#back 後退
            dutyCyclepercentage = position * 100 /ms_per_cycle
            print "position:" +str(position)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm.start(dutyCyclepercentage)
            time.sleep(0.5)
        
        for position1 in positionList1:#catch up 爪子上升
            dutyCyclepercentage = position1 * 100 /ms_per_cycle
            print "position:" +str(position1)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm1.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position5 in positionList5:#turn right 右轉
            dutyCyclepercentage = position5 * 100 /ms_per_cycle
            print "position:" +str(position5)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm5.start(dutyCyclepercentage)
            time.sleep(0.5)
    
        for position4 in positionList10:#down 下降
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position2 in positionList7:#put 放
            dutyCyclepercentage = position2 * 100 /ms_per_cycle
            print "position:" +str(position2)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm2.start(dutyCyclepercentage)
            time.sleep(0.5)  
        
        for position4 in positionList4:#up 上升
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
        
        for position5 in positionList11:#turn mid 回到原點
            dutyCyclepercentage = position5 * 100 /ms_per_cycle
            print "position:" +str(position5)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm5.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position1 in positionList14:#catch down 爪子下降
            dutyCyclepercentage = position1 * 100 /ms_per_cycle
            print "position:" +str(position1)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm1.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position4 in positionList10:#down 下降
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
            
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    elif((x1 == 0)&(x2 > 0)): #如果有藍色物體       
    #blue green 藍色移動方式
        while((xc-36) > x2):#left direction 左轉
            for position9 in positionList9:
                dutyCyclepercentage = position9 * 100 /ms_per_cycle
                print "position:" +str(position9)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm5.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList9[0] = positionList9[0] + 0.01
                c = c+1
                
            for position in positionList:#<> 前進
                dutyCyclepercentage = position * 100 /ms_per_cycle

                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList[0] = positionList[0] + 0.01
                xx = xx+1
                                      
        while((xc+36) < x2): #right direction 右轉
            
            for position9 in positionList9:
                dutyCyclepercentage = position9 * 100 /ms_per_cycle
                print "position:" +str(position9)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm5.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList9[0] = positionList9[0] - 0.01
                c = c+1

            for position in positionList:#<> 前進
                dutyCyclepercentage = position * 100 /ms_per_cycle

                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList[0] = positionList[0] + 0.01
                xx = xx+1
                     
        while((yc-y2) > 45):
            for position in positionList:#<> 前進 
                dutyCyclepercentage = position * 100 /ms_per_cycle
                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm.start(dutyCyclepercentage)
                time.sleep(0.2)
                positionList[0] = positionList[0] + 0.1
                ss = ss+1
                    
            for position in positionList8:#catch up 爪子上升
                dutyCyclepercentage = position * 100 /ms_per_cycle
                print "position:" +str(position)
                print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
                print ""
                pwm1.start(dutyCyclepercentage)
                time.sleep(0.2)
            
            if((yc-y2) <= 45):
                del xb,xc,yb,yc#刪除數值
                client.loop_stop()#停止接收數值
                del x2,y2 #刪除數值
                s1 = s1 - 1 #總物體數扣1
                break
            
        #回歸原本數值位置
        positionList[0] = positionList[0] - (0.01*xx) - (0.1*ss)
        if(positionList9[0] > 1.4):
            positionList9[0] = positionList9[0] - (0.01*c)
        if(positionList9[0] < 1.4):
            positionList9[0] = positionList9[0] + (0.01*c)
        #-------------------------------------------------------------------
        #fixed move固定動作
        for position2 in positionList2:#catch 抓取
            dutyCyclepercentage = position2 * 100 /ms_per_cycle
            print "position:" +str(position2)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm2.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position4 in positionList4:#up 上升
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
                        
        for position in positionList6:#back 後退
            dutyCyclepercentage = position * 100 /ms_per_cycle
            print "position:" +str(position)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm.start(dutyCyclepercentage)
            time.sleep(0.5)

        for position1 in positionList1:#catch up 爪子上升
            dutyCyclepercentage = position1 * 100 /ms_per_cycle
            print "position:" +str(position1)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm1.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position5 in positionList13:#turn left 左轉
            dutyCyclepercentage = position5 * 100 /ms_per_cycle
            print "position:" +str(position5)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm5.start(dutyCyclepercentage)
            time.sleep(0.3)
            
        for position4 in positionList10:#down 下降
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position2 in positionList7:#put 放
            dutyCyclepercentage = position2 * 100 /ms_per_cycle
            print "position:" +str(position2)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm2.start(dutyCyclepercentage)
            time.sleep(0.5)  
        
        for position4 in positionList4:#up 上升
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
        
        for position5 in positionList11:#turn mid 回到原點
            dutyCyclepercentage = position5 * 100 /ms_per_cycle
            print "position:" +str(position5)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm5.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position1 in positionList14:#catch down 爪子下降
            dutyCyclepercentage = position1 * 100 /ms_per_cycle
            print "position:" +str(position1)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm1.start(dutyCyclepercentage)
            time.sleep(0.5)
            
        for position4 in positionList10:#down 下降
            dutyCyclepercentage = position4 * 100 /ms_per_cycle
            print "position:" +str(position4)
            print "Duty Cycle:" +str(dutyCyclepercentage) + "%"
            print ""
            pwm4.start(dutyCyclepercentage)
            time.sleep(0.5)
            
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if(s2 > s1):#如果還有物體
        #接收誤差資料
        client.loop_start()
        time.sleep(2)
        client.loop_stop()
    elif(s == 0):#如果沒有物體
        client.loop_stop()#停止接收資料
        #關閉電源
        pwm5.stop()
        pwm.stop()
        pwm1.stop()
        pwm3.stop()
        pwm2.stop()
        pwm4.stop()
        
        
GPIO.cleanup()#清除GPIO


