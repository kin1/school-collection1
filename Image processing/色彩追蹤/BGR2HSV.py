# Import the necessary packages
import cv2
import numpy as np
pic5 = cv2.imread("pic5.png")
pic5HSV = cv2.cvtColor(pic5,cv2.COLOR_BGR2HSV)

# Show the image and wait for a keypress

cv2.imshow("BRG pic5", pic5)
cv2.imshow("HSV pic5", pic5HSV)

cv2.waitKey(0)

cv2.destroyAllWindows()

