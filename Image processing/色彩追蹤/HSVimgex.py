import cv2
import numpy as np

cap = cv2.VideoCapture('redball0007.avi')

while(1):
    ret, frame = cap.read()
    print frame.shape
    img = np.zeros(frame.shape, np.uint8)
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    # define range of blue color in HSV
    lower_red = np.array([0,100,100])
    upper_red = np.array([10,255,255])

    # Threshold the HSV image to get only blue colors
 
    mask1 = cv2.inRange(hsv, lower_red, upper_red)

    hsv1 = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    lower_red = np.array([160,100,100])
    upper_red = np.array([180,255,255])

    mask0 = cv2.inRange(hsv1, lower_red, upper_red)

    mask2 = cv2.add(mask0,mask1)
    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask=mask2)

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask2)
    cv2.imshow('res',res)
    
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break
    
cv2.destroyAllWindows()
