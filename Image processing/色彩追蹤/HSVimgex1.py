import cv2
import numpy as np

frame = cv2.imread('pic5.png',1)
print frame.shape

while(1):
    img = np.zeros(frame.shape, np.uint8)
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    # define range of blue color in HSV
    lower_blue = np.array([110,100,100])
    upper_blue = np.array([130,225,225])

    # Threshold the HSV image to get only blue colors
 
    mask1 = cv2.inRange(hsv, lower_blue, upper_blue)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask=mask1)

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask1)
    cv2.imshow('res',res)
    
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break
    
cv2.destroyAllWindows()
