# Import the necessary packages
import cv2
import numpy as np
fly = cv2.imread("fly.jpg")
flygray = cv2.cvtColor(fly,cv2.COLOR_BGR2GRAY)

# Show the image and wait for a keypress

cv2.imshow("BRG fly", fly)
cv2.imshow("fly gray", flygray)

cv2.waitKey(0)

cv2.destroyAllWindows()

